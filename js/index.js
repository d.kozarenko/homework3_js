"use sctrict";

let firstNumber = prompt("Enter first number: ");
let secondNumber = prompt("Enter second number: ");
while (firstNumber.trim() === "" || secondNumber.trim() === "" || isNaN(+firstNumber) || isNaN(+secondNumber)) {
    firstNumber = prompt("Enter first number: ", firstNumber);
    secondNumber = prompt("Enter second number: ", secondNumber);
}

let mathOperation = prompt("Enter math operation you want to do: (+, -, *, /): ");

console.log(mathCalc(+firstNumber, +secondNumber, mathOperation));
function mathCalc(a, b, sign) {
    switch (sign) {
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }
}
